import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { NewUserComponent } from './new-user/new-user.component';
import { FormsModule } from '@angular/forms';
import { UserService } from './shared/user.service';
import { CreateGroupComponent } from './create-group/create-group.component';
import { UsersGroupComponent } from './users-group/users-group.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    NewUserComponent,
    CreateGroupComponent,
    UsersGroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
