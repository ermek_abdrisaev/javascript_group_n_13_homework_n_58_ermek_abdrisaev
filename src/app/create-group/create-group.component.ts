import { Component, Input, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { UserGroup } from '../shared/group.item.model';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {

  @Input() user!: User;
  @Input() userGroups!: UserGroup[];

  constructor(public userService: UserService){}

  ngOnInit(): void {
  }

}
