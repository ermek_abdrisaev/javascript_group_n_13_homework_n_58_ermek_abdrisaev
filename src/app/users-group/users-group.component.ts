import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { UserGroup } from '../shared/group.item.model';

@Component({
  selector: 'app-users-group',
  templateUrl: './users-group.component.html',
  styleUrls: ['./users-group.component.css']
})
export class UsersGroupComponent {

  @Input() userGroup!: UserGroup;



  constructor(public userService: UserService){}


}
