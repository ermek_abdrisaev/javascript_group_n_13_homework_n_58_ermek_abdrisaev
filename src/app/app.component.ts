import { Component, EventEmitter, Output } from '@angular/core';
import { User } from './shared/user.model';
import { UserService } from './shared/user.service';
import { UserGroup } from './shared/group.item.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  users: User[] = [];

  userGroups: UserGroup[] = [];

  @Output() userAddedToGroup = new EventEmitter<User>();

  constructor(public userService: UserService){};

  onNewUser(user: User){
    this.userService.users.push(user);
  }

  onUserClick(user: User) {
    this.userAddedToGroup.emit(user);
  }

  onUserAddetToGroup(user: User) {
    const userGroup = new UserGroup(user);
    this.userGroups.push(userGroup);
  }
}
